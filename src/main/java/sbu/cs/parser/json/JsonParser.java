package sbu.cs.parser.json;

import java.util.ArrayList;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {

        // TODO implement this
        ArrayList<map> listOfMaps = new ArrayList<>();
        String data2 = commaToHash(data);
        String str = data2.replaceAll("[{\"}\n\t]" , "");
        String[] allPairs = str.split(",");
        str = hashToComma(str);
        for (int i = 0 ; i < allPairs.length ; i++){
            allPairs[i] = hashToComma(allPairs[i]);
            String[] pair = allPairs[i].split(":");
            map thisMap = new map(removeSpace(pair[0]),removeSpace(pair[1]));
            listOfMaps.add(thisMap);
        }
        Json maps = new Json(listOfMaps);
        return maps;
    }
    public static String commaToHash (String data){
        StringBuilder sb = new StringBuilder();
        sb.append(data);
        int index1 = 0 ;
        for (int i = 0 ; i < data.length() ; i ++){
            index1 = data.indexOf("[",index1);
            int index2 = data.indexOf("]" , index1);
            if (index2 != -1 && index1 != -1){
                for (int j = index1 ; j < index2 ; j ++){
                    if (sb.charAt(j)==','){
                        sb.setCharAt(j,'#');
                    }
                }
            }
            index1 = index2;
        }
        return sb.toString() ;
    }
    public static String hashToComma (String data2){
        StringBuilder sb = new StringBuilder();
        sb.append(data2);
        for (int i = 0 ; i < sb.length() ; i ++){
            if (sb.charAt(i) == '#'){
                sb.setCharAt(i,',');
            }
        }
        return sb.toString() ;
    }
    public static String removeSpace (String data) {
        StringBuilder sb = new StringBuilder();
        sb.append(data);
        while (sb.charAt(0) == ' '){
            sb.deleteCharAt(0);
        }
        return sb.toString();
    }
    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
