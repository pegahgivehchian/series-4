package sbu.cs.parser.json;

import java.util.ArrayList;

public class Json implements JsonInterface {
    private ArrayList<map> maps = new ArrayList<>();
    public Json(ArrayList maps){
        this.maps = maps ;
    }
    @Override
    public String getStringValue(String key) {
        // TODO implement this
        for ( int i = 0 ; i < maps.size() ; i ++){
            if (key.equals(maps.get(i).getKey())){
                return maps.get(i).getValue();
            }
        }

        return null;
    }
}
