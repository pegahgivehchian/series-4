package sbu.cs.parser.html;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class HTMLParser {

    /*
    * this function will get a String and returns a Dom object
     */
    public static Node parse(String document) {
        // TODO implement this
        String str = document.replaceAll("\n","");
        Node node = new Node(str);
        return node;
    }

    /*
    * a function that will return string representation of dom object.
    * only implement this after all other functions been implemented because this
    * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        // TODO implement this for more score
        return null;
    }
}
