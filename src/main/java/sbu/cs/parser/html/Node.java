package sbu.cs.parser.html;

import sbu.cs.parser.json.Json;
import sbu.cs.parser.json.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface {
    private String data;
    public Node (String Str) {
        this.data = Str ;
    }
    /*
    * this function will return all that exists inside a tag
    * for example for <html><body><p>hi</p></body></html>, if we are on
    * html tag this function will return <body><p1>hi</p1></body> and if we are on
    * body tag this function will return <p1>hi</p1> and if we are on
    * p tag this function will return hi
    * if there is nothing inside tag then null will be returned
     */
    @Override
    public String getStringInside() {
        // TODO implement this
        StringBuilder sb = new StringBuilder();
        sb.append(this.data);
        if (hasTag(sb)){
            sb = removeTag(sb);
            return sb.toString();
        }
        else {
            return null;
        }
    }

    /*
    *
     */
    @Override
    public List<Node> getChildren (){
        List<Node> children = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(this.data);
        sb = this.removeTag(sb);
        while (( sb.indexOf("</") != -1) && (sb.indexOf(">") != -1))
        {
            int index1 = sb.indexOf("<");
            if (sb.charAt(index1 + 1)=='/'){
                int index2 = sb.indexOf(">",index1);
                String str = sb.substring(index1 , index2+1);
                Node child = new Node(str);
                sb.delete(index1,index2+1);
                children.add(child);
            }
            else if (hasTag(sb)) {
                int index2 = sb.indexOf(">");
                int indexSpace = sb.indexOf(" ",index1);
                if (indexSpace < index2){ // has attribute
                    String tag = sb.substring(index1 + 1 , indexSpace);
                    int index3 = sb.indexOf("</" + tag );
                    int index4 = index3 + tag.length() + 3;
                    Node child = new Node(sb.substring(index1, index4));
                    sb.delete(index1, index4 );
                    children.add(child);
                }
                else {
                    String tag = sb.substring(index1 + 1, index2);
                    int index3 = sb.indexOf("</" + tag);
                    int index4 = index3 + tag.length() + 3;
                    Node child = new Node(sb.substring(index1, index4));
                    sb.delete(index1, index4);
                    children.add(child);
                }
            }
            else{
                children.add(null);
            }
        }
        return children;
    }
    public StringBuilder removeTag (StringBuilder sb){ //removes tag of node we're in
        int index1 = sb.indexOf("<");
        int index2 = sb.indexOf(">");
        int indexSpace = sb.indexOf(" ");
        String tag;
        if (indexSpace < index2){
            tag = sb.substring(index1 + 1, indexSpace);
            sb.delete(index1,index2 + 1);
        }
        else {
            tag = sb.substring(index1 + 1, index2);
            sb.delete(index1,index2 + 1);
        }
        int index3 = sb.indexOf("</" + tag);
        sb.delete(index3 , index3 + tag.length() + 3);
        return sb;
    }
    public boolean hasTag(StringBuilder sb){
        StringBuilder sb2 = new StringBuilder();
        sb2.append(sb);
        if ( sb2.indexOf("<")!= -1 && sb2.indexOf(">")!= -1 ){
            sb2.setCharAt(sb.indexOf("<"),'{');
            sb2.setCharAt(sb.indexOf(">"),'}');
            if ( sb2.indexOf("</")!= -1 && sb2.indexOf(">")!= -1 ){
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }
    /*
    * in html tags all attributes are in key value shape. this function will get a attribute key
    * and return it's value as String.
    * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key){
        int index1 = this.data.indexOf("<");
        int index2 = this.data.indexOf(">" , index1);
        int indexSpace = this.data.indexOf(" ",index1);
        String str = this.data.substring(indexSpace + 1 , index2);
        str = spaceToHash(str);
        String[] pairs = str.split(" ");
        Map<String,String> pair = new HashMap<>();
        for (int i = 0 ; i < pairs.length ; i ++){
            pair.put(pairs[i].split("=")[0],pairs[i].split("=")[1]);
        }
        String answer = pair.get(key).replaceAll("\"" , "");
        return answer.replaceAll("#"," ");
    }
    public String spaceToHash (String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        int index1 = 0;
        for (int i = 0 ; i < sb.length() ; i ++){
            index1 = sb.indexOf("\"" , index1 + 1) ;
            int index2 = sb.indexOf("\"" , index1 + 1);
            if (index2 != -1 && index1 != -1) {
                for (int j = index1; j < index2; j++) {
                    if (sb.charAt(j) == ' ') {
                        sb.setCharAt(j, '#');
                    }
                }
            }
            else{
                break;
            }
            index1 = index2;

        }
        return sb.toString() ;
    }
}
